<html>
    <head>
        <script type="text/javascript" src="{{ asset('js/app.js')}}"></script>
        <link rel="stylesheet" href="{{ asset('css/app.css')}}">
    </head>
    <body>
        <div class="container">
        <h1>Lista Posts</h1>
        <div id="posts-list">
            <ul></ul>
        </div>
    </body>

    <script type="text/javascript">
        window.Laravel = {};
        window.Laravel.api_token = '{{ auth()->user()->api_token }}';

        $(function(){
            axios.get('/api/posts',{
                params: {
                    'api.token' : Laravel.api_token
                }
            })
            .then((response) => {
                const posts = response.data;
                posts.forEach((post) => {
                    const li = $('<li>').text(post.title);
                    $("#posts-list ul").append(li);
                })
            });
        });
    </script>
</html>
