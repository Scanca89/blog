<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Post;
use App\User;

class PostPostsTest extends TestCase
{
    use RefreshDatabase;

    public function test_anonymous_user_cannot_create_post(){
        $response = $this->json('POST', '/api/posts', [
            'title'=>'Titolo di prova',
            'body'=>'Testo di prova'
        ]);
        $response->assertStatus(401);
        $posts = Post::all();
        $this->assertEquals(0, $posts->count());
    }

    public function test_reader_cannot_create_post(){
        $user = factory(User::class)->create([
            'role'=>'reader'
        ]);
        //chiamare come se fossi l'utente X usando api_token
        $response = $this->actingAs($user, 'api')->json('POST', '/api/posts', [
            'title'=>'Titolo di prova',
            'body'=>'Testo di prova'
        ]);
        $response->assertStatus(403);
        $posts = Post::all();
        $this->assertEquals(0, $posts->count());
    }

    public function test_admin_can_create_post(){
        $user = factory(User::class)->create([
            'role'=>'admin'
        ]);
        //chiamare come se fossi l'utente X usando api_token
        $response = $this->actingAs($user, 'api')->json('POST', '/api/posts', [
            'title'=>'Titolo di prova',
            'body'=>'Testo di prova'
        ]);
        $response->assertStatus(200);
        $posts = Post::all();
        $this->assertEquals(1, $posts->count());
        $content = json_decode($response->getContent());
        $this->assertEquals($posts->first()->id, $content->id);
        $this->assertEquals($posts->first()->title, $content->title);
        $this->assertEquals($posts->first()->body, $content->body);
    }

    public function test_editor_can_create_post(){
        $user = factory(User::class)->create([
            'role'=>'editor'
        ]);
        //chiamare come se fossi l'utente X usando api_token
        $response = $this->actingAs($user, 'api')->json('POST', '/api/posts', [
            'title'=>'Titolo di prova',
            'body'=>'Testo di prova'
        ]);
        $response->assertStatus(200);
        $posts = Post::all();
        $this->assertEquals(1, $posts->count());
        $content = json_decode($response->getContent());
        $this->assertEquals($posts->first()->id, $content->id);
        $this->assertEquals($posts->first()->title, $content->title);
        $this->assertEquals($posts->first()->body, $content->body);
    }
}
