<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeletePostTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp() : void{
        parent::setUp();
        $this->admin = factory(User::class)->create(['role' => 'admin']);
        $this->editor1 = factory(User::class)->create(['role' => 'editor']);
        $this->editor2 = factory(User::class)->create(['role' => 'editor']);
        $this->reader = factory(User::class)->create(['role' => 'reader']);

        $this->editor1Post = factory(Post::class)->create(['user_id' => $this->editor1->id]);
        $this->editor2Post = factory(Post::class)->create(['user_id' => $this->editor2->id]);
    }

    // public function test_anonymous_user_cannot_delete_post(){
    //     $response = $this->json('DELETE', '/api/posts/' . $this->editor1Post->id, [
    //         'title' => 'Titolo Aggiornato'
    //     ]);
    //     $response->assertStatus(401);
    //     $post = Post::find($this->editor1Post->id);
    //     $this->assertNotNull($post);
    // }

    // public function test_reader_cannot_delete_post(){
    //     $response = $this->json('DELETE', '/api/posts/' . $this->editor1Post->id, [
    //         'title' => 'Titolo Aggiornato'
    //     ]);
    //     $response->assertStatus(403);
    //     $post = Post::find($this->editor1Post->id);
    //     $this->assertNotNull($post);
    // }

}
