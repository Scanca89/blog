<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \App\User;
use \App\Post;

class PutPostTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp() : void{
        parent::setUp();
        $this->admin = factory(User::class)->create(['role' => 'admin']);
        $this->editor1 = factory(User::class)->create(['role' => 'editor']);
        $this->editor2 = factory(User::class)->create(['role' => 'editor']);
        $this->reader = factory(User::class)->create(['role' => 'reader']);

        $this->editor1Post = factory(Post::class)->create(['user_id' => $this->editor1->id]);
        $this->editor2Post = factory(Post::class)->create(['user_id' => $this->editor2->id]);
    }

    public function test_anonymous_user_cannot_update_post(){
        $response = $this->json('PUT', '/api/posts/' . $this->editor1Post->id, [
            'title' => 'Titolo Aggiornato'
        ]);
        $response->assertStatus(401);
        $post = Post::find($this->editor1Post->id);
        $this->assertEquals($post->title, $this->editor1Post->title);
    }

    public function test_reader_cannot_update_post(){
        $response = $this->actingAs($this->reader,'api')->
        json('PUT', '/api/posts/' . $this->editor1Post->id, [
            'title' => 'Titolo Aggiornato'
        ]);
        $response->assertStatus(403);
        $post = Post::find($this->editor1Post->id);
        $this->assertEquals($post->title, $this->editor1Post->title);

    }

    public function test_editor_can_update_his_post_title(){
        $response = $this->actingAs($this->editor1,'api')->
        json('PUT', '/api/posts/' . $this->editor1Post->id, [
            'title' => 'Titolo Aggiornato'
        ]);
        $response->assertStatus(200);
        $post = Post::find($this->editor1Post->id);
        $this->assertEquals($post->title, 'Titolo Aggiornato');
        $output = json_decode($response->getContent());
        $this->assertEquals('Titolo Aggiornato',$output->title);

    }

    public function test_editor_can_update_his_post_body(){
        $response = $this->actingAs($this->editor1,'api')->
        json('PUT', '/api/posts/' . $this->editor1Post->id, [
            'body' => 'Lorem Ipsum'
        ]);
        $response->assertStatus(200);
        $post = Post::find($this->editor1Post->id);
        $this->assertEquals($post->body, 'Lorem Ipsum');
        $output = json_decode($response->getContent());
        $this->assertEquals('Lorem Ipsum',$output->body);

    }

    public function test_editor_can_update_his_post_title_and_body(){
        $response = $this->actingAs($this->editor1,'api')->
        json('PUT', '/api/posts/' . $this->editor1Post->id, [
            'title' => 'Titolo Aggiornato',
            'body' => 'Lorem Ipsum'
        ]);
        $response->assertStatus(200);
        $post = Post::find($this->editor1Post->id);
        $this->assertEquals($post->title, 'Titolo Aggiornato');
        $this->assertEquals($post->body, 'Lorem Ipsum');
        $output = json_decode($response->getContent());
        $this->assertEquals('Titolo Aggiornato',$output->title);
        $this->assertEquals('Lorem Ipsum',$output->body);

    }

    public function test_editor_cannot_others_update_post(){
        $response = $this->actingAs($this->editor2,'api')->
        json('PUT', '/api/posts/' . $this->editor1Post->id, [
            'title' => 'Titolo Aggiornato'
        ]);
        $response->assertStatus(403);
        $post = Post::find($this->editor1Post->id);
        $this->assertEquals($post->title, $this->editor1Post->title);
        $this->assertEquals($post->title,$this->editor1Post->title);

    }

    public function test_editor_can_update_any_post(){
        $response = $this->actingAs($this->admin,'api')->
        json('PUT', '/api/posts/' . $this->editor1Post->id, [
            'title' => 'Titolo Aggiornato'
        ]);
        $response->assertStatus(200);
        $post = Post::find($this->editor1Post->id);
        $this->assertEquals($post->title, 'Titolo Aggiornato');
        $output = json_decode($response->getContent());
        $this->assertEquals('Titolo Aggiornato',$output->title);
    }
}
