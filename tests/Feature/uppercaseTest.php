<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Service\TestService;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class upeercaseServiceTest extends TestCase
{
    public function test_query_string_should_be_capitalized(){
        $this->mock('App\Services\TestService', function($mock) {
            $mock->shouldReceive('run')
                ->once()
                ->with('pippo')
                ->andReturns('PIPPO');
        });
        $response = $this->get('/test_uppercase?string=pippo');
        $response->assertStatus(200);
        $this->assertEquals('PIPPO', $response->getContent());
    }
}
