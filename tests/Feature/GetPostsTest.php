<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Post;

class GetPostsTest extends TestCase
{
    use RefreshDatabase;

    public function test_get_empty_post_list(){
        //fai la richiesta alla lista dei post
        //$this->get('/api/posts');

        //restituisce la risposta già parsata in json
        $response = $this->json('GET', '/api/posts');
        $response->assertStatus(200);
        $response->assertJson([]);
    }

    public function test_get_post_list_with_post_in_database(){
        $post = Post::create([
            'title' => 'Titolo 1',
            'body' => 'Questo è il mio primo post',
        ]);

        $response = $this->json('GET', '/api/posts');
        $response->assertStatus(200);
        $response->assertJsonCount(1);
        $content = json_decode($response->getContent());
        $this->assertEquals($post->id, $content[0]->id);
        $this->assertEquals($post->title, $content[0]->title);
        $this->assertEquals($post->body, $content[0]->body);
    }
}
