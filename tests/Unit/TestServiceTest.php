<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

ini_set('memory_limit','512M');

class TestServiceTest extends TestCase
{
    //mock = sono oggetti finti che uso in fase di test

    public function test_string_should_be_capitalazier(){
        $service = app()->make('App\Services\TestService');
        $this->assertEquals('PIPPO', $service->run('pippo'));
    }
}
