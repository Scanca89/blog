<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class LoginAs
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $id)
    {
        Auth::loginUsingId($id);
        return $next($request);
    }
}
