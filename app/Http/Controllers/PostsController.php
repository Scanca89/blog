<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostsController extends Controller
{
    //

    //GATES = usate per dare il permesso di aggiungere un post
    //POLICY = usato per modificare un post (ho una risosra da modificare)

    public function index(){
        $posts = Post::all();
        return response()->json($posts);
    }

    public function store(Request $request){
        //GATES
        $this->authorize('create-posts');
        $post = Post::create($request->all());
        return response()->json($post);
    }

    public function update(Request $request, Post $post){
        $this->authorize('update', $post);
        $post->update($request->all());
        return response()->json($post);
    }

    public function destroy(Post $post){
        //posso lasciare l'update perchè se posso modificare un post lo posso anche cancellare.
        //se voglio cambiare regole devo aggiungere una policy nuova
        $this->authorize('update', $post);
        $post->delete();
        return response()->json(null);
    }

    public function indexHTML(){
        return view('posts.index');
    }

    public function testUppercase(Request $request){
        $string = $request->query('string');
        $service = app()->make('App\Services\TestService');
        return $service->run($string);
    }
}
