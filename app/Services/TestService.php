<?php

namespace App\Services;

class TestService{

    public function run($name){
        return strtoupper($name);
    }

}
