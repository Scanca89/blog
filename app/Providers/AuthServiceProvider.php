<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\User;
use App\Post;
use App\Policies\PostPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Post::class => PostPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('create-posts', function(User $user){
            return $user->role == 'admin' || $user->role == 'editor';
        });

        Gate::before(function(User $user, $action){
            if($user->role == 'admin'){
                return true;
            }
            //con null delego l'autorizzazione agli altri controlli
            return null;
        });
    }
}
