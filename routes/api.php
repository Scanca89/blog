<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/posts', 'PostsController@index');
Route::post('/posts', 'PostsController@store')->middleware('auth:api');
Route::put('/posts/{post}', 'PostsController@update')->middleware('auth:api');
Route::delete('/posts/{post}', 'PostsController@destroy')->middleware('auth:api');
